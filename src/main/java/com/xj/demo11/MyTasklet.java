package com.xj.demo11;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

/**
 * @Author : xjfu
 * @Date : 2021/12/12 15:37
 * @Description :demo11的Tasklet
 */
public class MyTasklet implements Tasklet {
    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {

        System.out.println("demo11 的 Tasklet 执行啦！");

        return RepeatStatus.FINISHED;
    }
}
