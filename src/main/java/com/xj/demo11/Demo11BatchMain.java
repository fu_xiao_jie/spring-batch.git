package com.xj.demo11;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @Author : xjfu
 * @Date : 2021/12/12 15:12
 * @Description :Step级拦截器
 */
public class Demo11BatchMain {

    public static void main(String[] args) {
        Demo11BatchMain batchMain = new Demo11BatchMain();

        //测试拦截器功能
        batchMain.executeJob("demo11/job/demo11-job.xml", "billJob1", "jobLauncher", new JobParametersBuilder().toJobParameters());

        //测试merge功能
        batchMain.executeJob("demo11/job/demo11-job.xml", "billJob2", "jobLauncher", new JobParametersBuilder().toJobParameters());
    }

    public void executeJob(String jobXmlPath, String jobId, String jobLauncherId, JobParameters jobParameters){
        ApplicationContext context = new ClassPathXmlApplicationContext(jobXmlPath);
        //获得JobLauncher
        JobLauncher jobLauncher = (JobLauncher) context.getBean(jobLauncherId);
        //获取要执行的Job
        Job job = (Job)context.getBean(jobId);

        try{
            //开始执行Job
            JobExecution jobExecution = jobLauncher.run(job, jobParameters);
            //输出执行结果
            System.out.println(jobExecution.toString());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
