package com.xj.demo11;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;

/**
 * @Author : xjfu
 * @Date : 2021/12/12 15:18
 * @Description :通过实现StepExecutionListener方式实现step级拦截器
 */
public class SysoutListener implements StepExecutionListener {
    @Override
    public void beforeStep(StepExecution stepExecution) {

        System.out.println("SysoutListener————beforeStep " + stepExecution.getStartTime());

    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {

        System.out.println("SysoutListener————afterStep " + stepExecution.getStartTime());

        return null;
    }
}
