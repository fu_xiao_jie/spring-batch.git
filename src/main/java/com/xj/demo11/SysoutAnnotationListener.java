package com.xj.demo11;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.AfterStep;
import org.springframework.batch.core.annotation.BeforeStep;

/**
 * @Author : xjfu
 * @Date : 2021/12/12 15:19
 * @Description :通过注解的方式实现step级拦截器
 */
public class SysoutAnnotationListener {

    @BeforeStep
    public void beforeStep(StepExecution stepExecution){
        System.out.println("SysoutAnnotationListener————beforeStep " + stepExecution.getStartTime());
    }

    @AfterStep
    public ExitStatus afterStep(StepExecution stepExecution){
        System.out.println("SysoutAnnotationListener————afterStep " + stepExecution.getStartTime());

        return null;
    }
}
