package com.xj.demo23;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

/**
 * 借贷卡数据转换类，将30*格式的记录转换为领域对象DebitBill
 */
public class DebitBillFieldSetMapper implements FieldSetMapper<DebitBill> {

	public DebitBill mapFieldSet(FieldSet fieldSet) throws BindException {
		DebitBill result = new DebitBill();
		result.setAccountID(fieldSet.readString("accountID"));
		result.setName(fieldSet.readString("name"));
		result.setAmount(fieldSet.readDouble("amount"));
		result.setDate(fieldSet.readString("date"));
		return result;
	}
}
