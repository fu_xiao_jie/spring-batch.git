package com.xj.demo4;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Date;

/**
 * @Author : xjfu
 * @Date : 2021/11/11 8:48
 * @Description :Job 参数校验启动类
 */
public class Demo4BatchMain {

    public static void main(String[] args) {
        Demo4BatchMain batchMain = new Demo4BatchMain();
        System.out.println("**************第一次执行  Begin**************");

        //不会报错，因为要求必须要有date参数
        batchMain.executeJob("demo4/job/demo4-job.xml", "demo4Job", "jobLauncher",new JobParametersBuilder().addDate("date",new Date()).toJobParameters());

        System.out.println("**************第一次执行  End**************");

        System.out.println("**************第二次执行  Begin**************");

        //会报错，因为没有规定有“test”参数
        batchMain.executeJob("demo4/job/demo4-job.xml", "demo4Job", "jobLauncher",new JobParametersBuilder().addDate("date",new Date()).addString("test","test parameter").toJobParameters());

        System.out.println("**************第二次执行  End**************");
    }

    /**
     *执行Job
     * @param jobXmlPath 配置job的xml文件路径
     * @param jobId job的id
     * @param jobLauncherId jobLauncher的id
     * @param jobParameters 参数
     */
    public void executeJob(String jobXmlPath, String jobId, String jobLauncherId, JobParameters jobParameters){
        ApplicationContext context = new ClassPathXmlApplicationContext(jobXmlPath);
        //Spring Batch的作业启动器
        JobLauncher launcher = (JobLauncher) context.getBean(jobLauncherId);
        //获取要执行的Job
        Job job = (Job) context.getBean(jobId);

        try{
            //开始执行作业
            JobExecution result = launcher.run(job,jobParameters);
            //输出执行结果
            System.out.println(result.toString());
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
