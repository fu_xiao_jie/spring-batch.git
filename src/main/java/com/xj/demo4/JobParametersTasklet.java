package com.xj.demo4;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

/**
 * @Author : xjfu
 * @Date : 2021/11/11 8:50
 * @Description :Job 参数校验Tasklet
 */
public class JobParametersTasklet implements Tasklet {

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {

        System.out.println("进入 Job Parameters 的 Tasklet");

        return RepeatStatus.FINISHED;
    }
}
