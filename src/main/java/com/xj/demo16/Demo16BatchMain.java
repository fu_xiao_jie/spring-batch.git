package com.xj.demo16;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @Author : xjfu
 * @Date : 2021/12/27 17:24
 * @Description :chunk 完成策略
 */
public class Demo16BatchMain {
    public static void main(String[] args) {
        Demo16BatchMain batchMain = new Demo16BatchMain();
        //测试skip
        batchMain.executeJob("demo16/job/demo16-job.xml", "completionPolicyJob", "jobLauncher", new JobParametersBuilder().toJobParameters());
    }

    /**
     *执行Job
     * @param jobXmlPath 配置job的xml文件路径
     * @param jobId job的id
     * @param jobLauncherId jobLauncher的id
     * @param jobParameters 参数
     */
    public void executeJob(String jobXmlPath, String jobId, String jobLauncherId, JobParameters jobParameters){
        ApplicationContext context = new ClassPathXmlApplicationContext(jobXmlPath);
        JobLauncher jobLauncher = (JobLauncher) context.getBean(jobLauncherId);
        //获取要执行的Job
        Job job = (Job)context.getBean(jobId);

        try{
            //开始执行作业Job
            JobExecution jobExecution =  jobLauncher.run(job, jobParameters);
            //输出执行结果
            System.out.println(jobExecution.toString());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
