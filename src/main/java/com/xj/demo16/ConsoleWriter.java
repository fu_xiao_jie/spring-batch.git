package com.xj.demo16;

import org.springframework.batch.item.ItemWriter;

import java.util.List;

public class ConsoleWriter implements ItemWriter<String> {

    public void write(List<? extends String> items) throws Exception {
        System.out.println("Write begin:");
        for(String item : items){
            System.out.println("item:" + item);
        }
        System.out.println("Write end!!");
    }

}
