package com.xj.demo29;

import org.springframework.jdbc.core.PreparedStatementSetter;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class CreditBillPreparedStatementSetter implements
        PreparedStatementSetter {

	public void setValues(PreparedStatement ps) throws SQLException {
		//此处设置第一个参数为“5”
		ps.setString(1, "5");
	}

}
