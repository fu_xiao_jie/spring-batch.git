package com.xj.demo9;

import org.springframework.batch.core.JobParameters;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
/**
 * @Author : xjfu
 * @Date : 2021/11/18 8:37
 * @Description :Job与定时任务集成
 */
public class Demo9BatchMain {

    public static void main(String[] args) {
        Demo9BatchMain batchMain = new Demo9BatchMain();

        batchMain.executeJob("demo9/job/job-spring-scheduler.xml", "schedulerLauncher", "helloWorldJob", new JobParameters());
    }

    public void executeJob(String jobXmlPath, String jobLauncherId, String jobId, JobParameters jobParameters){

        //获取Job的配置文件
        ApplicationContext context = new ClassPathXmlApplicationContext(jobXmlPath);
        //获取JobLauncher
        SchedulerLauncher schedulerLauncher = (SchedulerLauncher) context.getBean(jobLauncherId);

        try {
            schedulerLauncher.launcher();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
