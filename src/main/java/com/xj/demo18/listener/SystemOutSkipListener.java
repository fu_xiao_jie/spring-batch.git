package com.xj.demo18.listener;

import org.springframework.batch.core.SkipListener;

public class SystemOutSkipListener implements SkipListener<String, String> {

	public void onSkipInRead(Throwable t) {
		System.out.println("SkipListener.onSkipInRead()");		
	}

	public void onSkipInWrite(String item, Throwable t) {
		System.out.println("SkipListener.onSkipInWrite()");
	}

	public void onSkipInProcess(String item, Throwable t) {
		System.out.println("SkipListener.onSkipInProcess()");		
	}

}
