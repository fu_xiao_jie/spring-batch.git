package com.xj.demo18.listener;

import org.springframework.batch.core.ItemReadListener;

public class SystemOutItemReadListener implements ItemReadListener<String> {

	public void beforeRead() {
		System.out.println("ItemReadListener.beforeRead()");
	}

	public void afterRead(String item) {
		System.out.println("ItemReadListener.afterRead()");
	}

	public void onReadError(Exception ex) {
		System.out.println("ItemReadListener.onReadError()");
	}

}
