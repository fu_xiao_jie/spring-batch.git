package com.xj.demo18;

import org.springframework.batch.item.ItemProcessor;

/**
 * @Author : xjfu
 * @Date : 2021/12/29 9:21
 * @Description :
 */
public class PassProcessor<T> implements ItemProcessor<T,T> {
    @Override
    public T process(T t) throws Exception {
        System.out.println("processor:" + (String) t);
        return t;
    }
}
