package com.xj.demo26;

import org.springframework.batch.item.file.FlatFileHeaderCallback;
import org.springframework.batch.item.file.LineCallbackHandler;
import java.io.IOException;
import java.io.Writer;
/**
 * @Author : xjfu
 * @Date : 2023/07/13 10:09
 * @Description : 自定义实现LineCallbackHandler，负责将跳过的文件内容写入到目标文件中
 */
public class CopyHeaderLineCallbackHandler implements LineCallbackHandler,
        FlatFileHeaderCallback {
	private String header = "";

	//操作handleLine()在每次跳过行时均会被触发，负责收集读文件跳过的头记录
	public void handleLine(String line) {
		this.header = line;
	}

	//负责将handleLine()收集到的记录写入到目标文件中
	public void writeHeader(Writer writer) throws IOException {
		writer.write(header);
	}
}
