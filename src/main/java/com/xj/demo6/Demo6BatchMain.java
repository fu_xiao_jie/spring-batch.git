package com.xj.demo6;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @Author : xjfu
 * @Date : 2021/11/17 8:14
 * @Description :Step Scope和Late Binding技术校验
 */
public class Demo6BatchMain {

    public static void main(String[] args) {
        Demo6BatchMain batchMain = new Demo6BatchMain();
        batchMain.executeJob("demo6/job/demo6-job.xml", "jobLauncher", "demo6Job", new JobParametersBuilder().addString("inputFilePath","demo6/data/demo6-inputFile.csv").toJobParameters());
    }

    /**
     * 执行Job
     * @param jobXmlPath 配置文件的路径
     * @param jobLauncherId launcher的id
     * @param jobId Job的id
     * @param jobParameters Job的参数
     */
    public void executeJob(String jobXmlPath, String jobLauncherId, String jobId, JobParameters jobParameters){
        ApplicationContext context = new ClassPathXmlApplicationContext(jobXmlPath);
        //Spring Batch启动器
        JobLauncher launcher = (JobLauncher)context.getBean(jobLauncherId);
        //获取Job
        Job job = (Job) context.getBean(jobId);

        try{
            //开始执行这个作业，获得处理结果
            JobExecution resut = launcher.run(job, jobParameters);
            System.out.println(resut.toString());
        }catch (Exception e){

        }
    }
}
