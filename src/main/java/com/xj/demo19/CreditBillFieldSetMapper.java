package com.xj.demo19;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

/**
 * @Author : xjfu
 * @Date : 2022/2/23 8:36
 * @Description :自定义实现CreditBillFieldSetMapper
 */
public class CreditBillFieldSetMapper implements FieldSetMapper<CreditBill> {
    @Override
    public CreditBill mapFieldSet(FieldSet fieldSet) throws BindException {

        CreditBill creditBill = new CreditBill();

        creditBill.setAccountID(fieldSet.readString("accountID"));
        creditBill.setName(fieldSet.readString("name"));
        creditBill.setAmount(fieldSet.readDouble("amount"));
        creditBill.setDate(fieldSet.readString("date"));
        creditBill.setAddress(fieldSet.readString("address"));

        return creditBill;
    }
}
