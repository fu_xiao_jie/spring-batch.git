package com.xj.demo15;

import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.retry.RetryListener;

public class SystemOutRetryListener implements RetryListener {

	//在进入retry之前执行该操作
	@Override
	public <T, E extends Throwable> boolean open(RetryContext retryContext, RetryCallback<T, E> retryCallback) {
		System.out.println("SystemOutRetryListener.open()");
		return true;
	}

	//在retry结束之前执行该操作
	@Override
	public <T, E extends Throwable> void close(RetryContext retryContext, RetryCallback<T, E> retryCallback, Throwable throwable) {
		System.out.println("SystemOutRetryListener.close()");
	}

	//重试发生错误时触发该操作
	@Override
	public <T, E extends Throwable> void onError(RetryContext retryContext, RetryCallback<T, E> retryCallback, Throwable throwable) {
		System.out.println("SystemOutRetryListener.onError()");
	}
}
