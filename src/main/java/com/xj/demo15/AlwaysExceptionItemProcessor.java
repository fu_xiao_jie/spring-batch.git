package com.xj.demo15;

import org.springframework.batch.item.ItemProcessor;
import java.util.Random;

public class AlwaysExceptionItemProcessor implements ItemProcessor<String, String> {
	Random ra = new Random();
	public String process(String item) throws Exception {
		int i = ra.nextInt(10);
		if(i%2 == 0){
			System.out.println("Process " + item + "; Random i=" + i +"; Exception:MockARuntimeException");
			throw new MockARuntimeException("make error!");
		}else{
			System.out.println("Process " + item + "; Random i=" + i +";");
			return item;
		}

	}
}
