package com.xj.demo15;

import java.util.List;
import org.springframework.batch.item.ItemWriter;

public class ConsoleWriter implements ItemWriter<String> {

    public void write(List<? extends String> items) throws Exception {
        System.out.println("Write begin:");
        for(String item : items){
            System.out.println("item:" + item);
        }
        System.out.println("Write end!!");
    }

}
