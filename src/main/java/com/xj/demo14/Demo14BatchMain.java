package com.xj.demo14;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @Author : xjfu
 * @Date : 2021/12/16 19:53
 * @Description :Step异常跳过
 */
public class Demo14BatchMain {
    public static void main(String[] args) {
        Demo14BatchMain batchMain = new Demo14BatchMain();
        //测试skip-limit和skippable-exception-classes组合
        //batchMain.executeJob("demo14/job/demo14-job.xml", "skipJob1", "jobLauncher", new JobParametersBuilder().toJobParameters());
        //测试skip-policy
        batchMain.executeJob("demo14/job/demo14-job.xml", "skipJob2", "jobLauncher", new JobParametersBuilder().toJobParameters());
    }

    /**
     *执行Job
     * @param jobXmlPath 配置job的xml文件路径
     * @param jobId job的id
     * @param jobLauncherId jobLauncher的id
     * @param jobParameters 参数
     */
    public void executeJob(String jobXmlPath, String jobId, String jobLauncherId, JobParameters jobParameters){
        ApplicationContext context = new ClassPathXmlApplicationContext(jobXmlPath);
        JobLauncher jobLauncher = (JobLauncher) context.getBean(jobLauncherId);
        //获取要执行的Job
        Job job = (Job)context.getBean(jobId);

        try{
            //开始执行作业Job
            JobExecution jobExecution =  jobLauncher.run(job, jobParameters);
            //输出执行结果
            System.out.println(jobExecution.toString());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
