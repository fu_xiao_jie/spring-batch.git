package com.xj.demo14;

import org.springframework.batch.core.step.skip.SkipLimitExceededException;
import org.springframework.batch.core.step.skip.SkipPolicy;

/**
 * @Author : xjfu
 * @Date : 2021/12/17 15:39
 * @Description :自定义跳过策略
 */
public class MySkipException implements SkipPolicy {
    @Override
    public boolean shouldSkip(Throwable throwable, int i) throws SkipLimitExceededException {

        //定义了RuntimeException和NullPointerException可以跳过
        if(throwable instanceof RuntimeException && throwable.getClass().getName() == RuntimeException.class.getName()){
            System.out.println(throwable.getClass().getName() + "跳过");
            return true;
        }else if(throwable instanceof NullPointerException && throwable.getClass().getName() == NullPointerException.class.getName()){
            System.out.println(throwable.getClass().getName() + "跳过");
            return true;
        }

        return false;
    }
}
