package com.xj.demo14;

import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
/**
 * @Author : xjfu
 * @Date : 2021/12/16 17:03
 * @Description :
 */
public class AutoReader implements ItemReader<String> {
    private int count = 0;
    private int maxCount = 30;
    @Override
    public String read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {

        if(count > maxCount){
            return null;
        }else{
            count++;
            System.out.println("reader--->" + count);
            return count + "";
        }
    }
}
