package com.xj.demo14;

import org.springframework.batch.item.ItemProcessor;
import java.util.Random;
/**
 * @Author : xjfu
 * @Date : 2021/12/16 19:45
 * @Description :
 */
public class AutoProcessor implements ItemProcessor<String, String> {
    Random r = new Random();
    @Override
    public String process(String item) throws Exception {
        int i = r.nextInt(10);

        if(i%2 == 0){
            System.out.println("Processor "+ item + "; Random i=" + i + "; Exception:MockARuntimeException");
            throw new RuntimeException("make error");
        }else{
            System.out.println("Processor "+ item + "; Random i=" + i);
            return item;
        }
    }
}
