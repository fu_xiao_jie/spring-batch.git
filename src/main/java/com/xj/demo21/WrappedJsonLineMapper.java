package com.xj.demo21;

import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.JsonLineMapper;
import java.util.Map;
/**
 * WrappedJsonLineMapper: 扩展实现，用于代理JsonLineMapper，负责将一条记录直接转换为领域对象CreditBill。
 * 						  WrappedJsonLineMapper将转换工作委托给JsonLineMapper，并将Map结果转换为CreditBill。
 */
public class WrappedJsonLineMapper implements LineMapper<CreditBill> {
	private JsonLineMapper delegate;

	public CreditBill mapLine(String line, int lineNumber) throws Exception {
		CreditBill result = new CreditBill();
		Map<String, Object> creditBillMap = delegate.mapLine(line, lineNumber);
		result.setAccountID((String)creditBillMap.get("accountID"));
		result.setName((String)creditBillMap.get("name"));
		result.setAmount((Double)creditBillMap.get("amount"));
		result.setDate((String)creditBillMap.get("date"));
		result.setAddress((String)creditBillMap.get("address"));
		return result;
	}

	public JsonLineMapper getDelegate() {
		return delegate;
	}

	public void setDelegate(JsonLineMapper delegate) {
		this.delegate = delegate;
	}
}
