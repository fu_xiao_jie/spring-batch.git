package com.xj.demo5;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

/**
 * @Author : xjfu
 * @Date : 2021/11/11 19:27
 * @Description :demo5的Tasklet
 */
public class MyTasklet implements Tasklet {
    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {

        System.out.println("**********Demo5的Tasklet执行啦*********");

        return RepeatStatus.FINISHED;
    }
}
