package com.xj.demo5;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @Author : xjfu
 * @Date : 2021/11/11 19:11
 * @Description :启动类
 */
public class Demo5BatchMain {

    public static void main(String[] args) {
        Demo5BatchMain batchMain = new Demo5BatchMain();
        //执行Job
        batchMain.executeJob("demo5/job/demo5-job.xml", "sonJob", "jobLauncher", new JobParameters());
    }

    /**
     *执行Job
     * @param jobXmlPath 配置job的xml文件路径
     * @param jobId job的id
     * @param jobLauncherId jobLauncher的id
     * @param jobParameters 参数
     */
    public void executeJob(String jobXmlPath, String jobId, String jobLauncherId, JobParameters jobParameters){

        //获得配置Job的配置文件
        ApplicationContext context = new ClassPathXmlApplicationContext(jobXmlPath);
        //获取JobLauncher
        JobLauncher launcher = (JobLauncher) context.getBean(jobLauncherId);
        //获取要执行的Job
        Job job = (Job) context.getBean(jobId);

        try{
            //开始执行Job
            JobExecution result = launcher.run(job, jobParameters);
            //输出执行结果
            System.out.println(result.toString());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
