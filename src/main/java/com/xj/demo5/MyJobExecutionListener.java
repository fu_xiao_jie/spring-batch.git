package com.xj.demo5;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;

/**
 * @Author : xjfu
 * @Date : 2021/11/7 18:53
 * @Description :实现JobExecutionListener接口的拦截器
 */
public class MyJobExecutionListener implements JobExecutionListener {

    //Job执行之前调用该方法
    @Override
    public void beforeJob(JobExecution jobExecution) {
        System.out.println("Father : MyJobExecutionListener——before： create time：" + jobExecution.getCreateTime());
    }

    //Job执行之后调用该方法
    @Override
    public void afterJob(JobExecution jobExecution) {
        System.out.println("Father : MyJobExecutionListener——after： create time：" + jobExecution.getCreateTime());
    }
}
