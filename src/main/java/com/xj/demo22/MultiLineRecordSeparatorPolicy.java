package com.xj.demo22;

import org.springframework.batch.item.file.separator.RecordSeparatorPolicy;

public class MultiLineRecordSeparatorPolicy implements RecordSeparatorPolicy {
	//定义读取的分隔符号
	private String delimiter = ",";
	//分割符总数，给定的字符串包含的分隔符个数等于此值，则认为是一条完整记录
	private int count = 0;

	//定义一条记录的完整规则
	public boolean isEndOfRecord(String line) {
		return countDelimiter(line) == count;
	}

	public String postProcess(String record) {
		return record;
	}

	public String preProcess(String record) {
		return record;
	}

	//统计给定内容包含分隔符的个数
	private int countDelimiter(String s) {
		String tmp = s;
		int index = -1;
		int count = 0;
		while ((index=tmp.indexOf(","))!=-1) {
			tmp = tmp.substring(index+1);
			count++;
		}
		return count;
	}

	public String getDelimiter() {
		return delimiter;
	}

	public int getCount() {
		return count;
	}

	public void setDelimiter(String delimiter) {
		this.delimiter = delimiter;
	}

	public void setCount(int count) {
		this.count = count;
	}
}
