package com.xj.demo13;

import org.springframework.batch.item.ItemProcessor;
/**
 * @Author : xjfu
 * @Date : 2021/12/16 19:45
 * @Description :
 */
public class AutoProcessor implements ItemProcessor<String, String> {

    @Override
    public String process(String item) throws Exception {
        System.out.println("Processor "+ item);
        return item;
    }
}
