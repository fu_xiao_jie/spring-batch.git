package com.xj.demo13;

import org.springframework.batch.item.ItemWriter;
import java.util.List;

/**
 * @Author : xjfu
 * @Date : 2021/12/16 19:49
 * @Description :
 */
public class AutoWriter implements ItemWriter<String> {
    @Override
    public void write(List<? extends String> list) throws Exception {
        System.out.println("Writer Begin!");
        for(String item : list){
            System.out.println("Writer --->" + item);
        }
        System.out.println("Writer End!");
    }
}
