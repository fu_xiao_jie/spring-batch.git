package com.xj.demo12;

import org.springframework.batch.item.ItemProcessor;
import java.util.Date;

/**
 * @Author : xjfu
 * @Date : 2021/10/26 19:29
 * @Description :
 */
public class CreditBillProcessor implements ItemProcessor<CreditBill, CreditBill> {
    @Override
    public CreditBill process(CreditBill bill) throws Exception {

        System.out.println(bill.toString());
        //做一些简单的处理
        bill.setAccountID(bill.getAccountID() + "1");
        bill.setName(bill.getName() + "2");
        bill.setAmount(bill.getAmount() + 3);
        bill.setDate(new Date().toString());
        bill.setAddress(bill.getAddress() + 5);

        return bill;
    }
}
