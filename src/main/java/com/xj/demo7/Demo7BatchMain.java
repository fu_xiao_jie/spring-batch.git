package com.xj.demo7;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @Author : xjfu
 * @Date : 2021/11/18 8:37
 * @Description :异步JobLauncher测试
 */
public class Demo7BatchMain {

    public static void main(String[] args) {
        Demo7BatchMain batchMain = new Demo7BatchMain();

        batchMain.executeJob("demo7/job/demo7-job.xml", "jobLauncher", "demo7Job", new JobParameters());
    }

    public  void executeJob(String jobXmlPath, String jobLauncherId, String jobId, JobParameters jobParameters){

        //获取Job的配置文件
        ApplicationContext context = new ClassPathXmlApplicationContext(jobXmlPath);
        //获取JobLauncher
        JobLauncher jobLauncher = (JobLauncher) context.getBean(jobLauncherId);
        //获取要执行的Job
        Job job = (Job)context.getBean(jobId);

        try {
            //执行Job
            JobExecution result = jobLauncher.run(job,jobParameters);
            //输出执行结果
            System.out.println(result.toString());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
