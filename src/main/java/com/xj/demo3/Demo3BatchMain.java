package com.xj.demo3;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @Author : xjfu
 * @Date : 2021/11/7 18:29
 * @Description :Spring Batch实现拦截器的两种方法测试的启动主类
 */
public class Demo3BatchMain {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("demo3/job/demo3-job.xml");
        //Spring Batch的作业启动器
        JobLauncher launcher = (JobLauncher) context.getBean("jobLauncher");
        //在demo3-jobContext.xml中配置一个作业
        Job job = (Job) context.getBean("demo3TaskletJob");
        
        try{
            //开始执行这个作业，获得出来结果(要运行的job,job参数对象)
            JobExecution result = launcher.run(job, new JobParameters());
            System.out.println(result.toString());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
