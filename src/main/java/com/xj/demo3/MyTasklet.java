package com.xj.demo3;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

/**
 * @Author : xjfu
 * @Date : 2021/11/7 18:29
 * @Description :
 */
public class MyTasklet implements Tasklet {
    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {

        System.out.println("This is a test for interceptor(拦截器)");

        return RepeatStatus.FINISHED;
    }
}
