package com.xj.demo3;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.annotation.AfterJob;
import org.springframework.batch.core.annotation.BeforeJob;

/**
 * @Author : xjfu
 * @Date : 2021/11/7 18:57
 * @Description :通过Annotation实现JobExecutionListener接口的拦截器
 */
public class MyAnnotationListener {

    //Job执行之前调用该方法
    @BeforeJob
    public void beforeJob(JobExecution jobExecution){
        System.out.println("MyAnnotationListener——before： create time：" + jobExecution.getCreateTime());

    }

    //Job执行之后调用该方法
    @AfterJob
    public void afterJob(JobExecution jobExecution){
        System.out.println("MyAnnotationListener——after： create time：" + jobExecution.getCreateTime());

    }
}
